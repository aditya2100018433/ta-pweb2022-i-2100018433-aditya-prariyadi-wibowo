<?php
    //Mendeklarasikan & Menampilkan Array
    $arrBuah=array("Jambu", "Anggur", "Semangka", "Mangga");
    echo $arrBuah[0]. "<br>"; //Jambu
    echo $arrBuah[3]. "<br><br>"; //Mangga

    $arrWarna=array();
    $arrWarna[]="Merah";
    $arrWarna[]="Jingga";
    $arrWarna[]="Kuning";
    $arrWarna[]="Hijau";
    echo $arrWarna[0]. "<br>"; //Merah
    echo $arrWarna[2]. "<br>"; //Kuning
?>