<html>
    <head>
        <title>Penanganan Form-Input</title>
        <link rel="stylesheet" href="program11.2.css">
    </head>
    <body>
        <div class="container">
            <form action="program11.2.3-Aditya.php" method="GET" name="input" onSubmit="validasi()">
                <br><h3 align="center">Pengisian Form Mahasiswa<br>Universitas Ahmad Dahlan</h3><pre>
        Nama          : <input type="text" name="nama" id="nama"/>
        Jenis Kelamin : <input type="radio" name="jk" id="jk" value="Laki-laki"/>Laki-laki  <input type="radio" name="jk" value="Perempuan"/>Perempuan
        Program Studi : <select name="prodi" id="prodi">
                        <option value="Teknik Informatika">Tenik Informatika</option>
                        <option value="Teknik Industri">Tenik Industri</option>
                        <option value="Teknik Elektro">Tenik Elektro</option>
                        <option value="Teknik Kimia">Tenik Kimia</option>
                        <option value="Teknologi Pangan">Teknologi Pangan</option>
                        </select>
        Hobi          : <input type="checkbox" name="hobi1" value="membaca"/>Membaca
                        <input type="checkbox" name="hobi2" value="menulis"/>Menulis
                        <input type="checkbox" name="hobi3" value="game"/>Game
                        <input type="checkbox" name="hobi4" value="olahraga"/>Olahraga
                        <input type="checkbox" name="hobi5" value="programing"/>Programing
        Angkatan      : 
                        <select name="angkatan" id="angkatan" size="2">
                        <option value="Tahun 2017">Tahun 2017</option>
                        <option value="Tahun 2018">Tahun 2018</option>
                        <option value="Tahun 2019">Tahun 2019</option>
                        <option value="Tahun 2020">Tahun 2020</option>
                        <option value="Tahun 2021">Tahun 2021</option>
                        </select>
        Alamat        : 
                        <textarea name="alamat" id="alamat" cols="20" rows="3"></textarea>

        <input class="msg-btn"  type="submit" name="Input" value="Kirim"/> <input class="follow-btn" type="reset" name="reset" value="Reset"/><br>
            </pre></form>
            <script type="text/javascript">
                function validasi(){
                    var nama=document.getElementById("nama").value;
                    var gender=document.getElementById("jk").value;
                    var prodi=document.getElementById("prodi").value;
                    var angkatan=document.getElementById("angkatan").value;
                    var alamat=document.getElementById("alamat").value;
                    if(nama !="" && gender !="" && prodi !="" && angkatan !="" && alamat !=""){
                        return true;
                    }else{
                        alert("Anda harus mengisi data dengan lengkap!");
                    }
                }
            </script>
        </div>
    </body>
</html>