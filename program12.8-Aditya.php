<?php
    //Mengurutkan array dengan asort() & arsort()
    $arrNilai = array("Aditya"=>80,"Ardian"=>90,"Rasya"=>75,"Keysha"=>85);
    echo "<b>Array sebelum Diurutkan</b>";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";

    asort($arrNilai);
    reset($arrNilai);
    echo "<b>Array setelah Diurutkan dengan asort()</b>";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";

    arsort($arrNilai);
    reset($arrNilai);
    echo "<b>Array setelah Diurutkan dengan arsort()</b>";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";
?>