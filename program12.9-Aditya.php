<?php
    //Menampilkan array dengan ksort & krsort
    $arrNilai = array("Aditya"=>80,"Ardian"=>90,"Rasya"=>75,"Keysha"=>85);
    echo "<b>Array sebelum Diurutkan</b>";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";

    ksort($arrNilai);
    reset($arrNilai);
    echo "<b>Array setelah Diurutkan dengan ksort()</b>";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";

    krsort($arrNilai);
    reset($arrNilai);
    echo "<b>Array setelah Diurutkan dengan krsort()</b>";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";
?>