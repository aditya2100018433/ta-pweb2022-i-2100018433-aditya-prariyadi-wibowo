<?php   
    //Menampilkan seluruh isi array dengan For dan Foreach
    $arrWarna=array("Red", "Orange", "Yellow", "Green", "Blue", "Purple");

    echo "Menampilkan isi array dengan For : <br>";
    for($i=0; $i<count($arrWarna); $i++){
        echo "Warna pelamgi <font color=$arrWarna[$i]>". $arrWarna[$i]."</font><br>";
    }

    echo "<br>Menampilkan isi array dengan Foreach : <br>";
    foreach($arrWarna as $warna){
        echo "Warna pelangi <font color=$warna>". $warna."</font><br>";
    }
?>