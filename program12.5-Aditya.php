<?php
    //Menampilkan isi array dengan foreach dan while-list
    $arrNilai=array("Aditya" => 90, "Ardian" => 80, "Rasya" => 85, "Keysha" => 75);
    echo "Menampilkan isi array asosiatif dengan foreach : <br>";
    foreach($arrNilai as $nama=>$nilai){
        echo "Nilai $nama=$nilai<br>";
    }

    reset($arrNilai);
    echo "<br>Menampilkan isi array asosiatif dengan WHILE dan LIST : <br>";
    while(list($nama,$nilai) = each($arrNilai)){
        echo "Nilai $nama=$nilai<br>";
    }
?>